Source: python-cement
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Michael Fladischer <fladi@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3-all,
               python3-argcomplete,
               python3-colorlog,
               python3-configobj,
               python3-genshi,
               python3-jinja2,
               python3-mock,
               python3-nose,
               python3-pyinotify,
               python3-pylibmc,
               python3-pystache,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme,
               python3-tabulate,
               python3-watchdog,
               python3-yaml
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-cement
Vcs-Git: https://salsa.debian.org/python-team/packages/python-cement.git
Homepage: https://builtoncement.com/

Package: python3-cement
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Recommends: python3-argcomplete,
            python3-colorlog,
            python3-configobj,
            python3-genshi,
            python3-jinja2,
            python3-pyinotify,
            python3-pylibmc,
            python3-pystache,
            python3-tabulate,
            python3-watchdog,
            python3-yaml
Suggests: python-cement-doc
Description: CLI Application Framework (Python3 version)
 Cement is an advanced CLI Application Framework for Python. Its goal is to
 introduce a standard, and feature-full platform for both simple and complex
 command line applications as well as support rapid development needs without
 sacrificing quality. Cement is flexible, and it’s use cases span from the
 simplicity of a micro-framework to the complexity of a mega-framework.
 .
 Features include:
  * Core pieces of the framework are customizable via handlers/interfaces
  * Extension handler interface to easily extend framework functionality
  * Config handler supports parsing multiple config files into one config
  * Argument handler parses command line arguments and merges with config
  * Log handler supports console and file logging
  * Plugin handler provides an interface to easily extend your application
  * Hook support adds a bit of magic to apps and also ties into framework
  * Handler system connects implementation classes with Interfaces
  * Output handler interface renders return dictionaries to console
  * Cache handler interface adds caching support for improved performance
  * Controller handler supports sub-commands, and nested controllers
 .
 This package contains the Python 3 version of the library.

Package: python-cement-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: CLI Application Framework (Documentation)
 Cement is an advanced CLI Application Framework for Python. Its goal is to
 introduce a standard, and feature-full platform for both simple and complex
 command line applications as well as support rapid development needs without
 sacrificing quality. Cement is flexible, and it’s use cases span from the
 simplicity of a micro-framework to the complexity of a mega-framework.
 .
 Features include:
  * Core pieces of the framework are customizable via handlers/interfaces
  * Extension handler interface to easily extend framework functionality
  * Config handler supports parsing multiple config files into one config
  * Argument handler parses command line arguments and merges with config
  * Log handler supports console and file logging
  * Plugin handler provides an interface to easily extend your application
  * Hook support adds a bit of magic to apps and also ties into framework
  * Handler system connects implementation classes with Interfaces
  * Output handler interface renders return dictionaries to console
  * Cache handler interface adds caching support for improved performance
  * Controller handler supports sub-commands, and nested controllers
 .
 This package contains the documentation.
